
###############################################################################

@Sparkle.register_sensor('dht', "Adafruit DHT adapter")
class Movement(Sensor):
	'''
    usage: sudo ./Adafruit_DHT.py [11|22|2302] GPIOpin#
	example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO #4
    '''

    mapping = {
        '11':   Adafruit_DHT.DHT11,
		'22':   Adafruit_DHT.DHT22,
		'2302': Adafruit_DHT.AM2302,
    }

    def initialize(self, pin, kind):
        self._sen = self.mapping[kind]
        self._pin = int(pin)

    def read(self):
        h, t = Adafruit_DHT.read_retry(self.sensor, pin)

        if h is not None and t is not None:
            yield 'humid', h
            yield 'temp',  t

        	print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)
        else:
        	print 'Failed to get reading. Try again!'
