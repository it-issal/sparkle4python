class HookNotFound(Exception):
    def __init__(self, componenet, hook, args=tuple([]), kwargs={}):
        super(Exception, self).__init__()

        self._comp   = component
        self._name   = hook
        self._args   = args
        self._kwargs = kwargs

    component = property(lambda self: self._comp)
    hook_name = property(lambda self: self._name)
    args      = property(lambda self: self._args)
    kwargs    = property(lambda self: self._kwargs)

