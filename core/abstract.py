class Primitive(object):
    def __init__(self, manager, narrow, *args, **kwargs):
        self._mgr   = manager
        self._nrw   = narrow
        self._hooks = {}

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

        if hasattr(self, 'reset'):
            if callable(self.reset):
                self.reset()
        elif hasattr(self, 'connect'):
            if callable(self.connect):
                self.connect()
    
    manager = property(lambda self: self._mgr)
    narrow  = property(lambda self: self._nrw)
    
    def hook(self, func):
        if callable(func):
            if func.__name__.startswith('process_'):
                nrw = func.__name__[len('process_'):]

                if len(nrw) and (nrw not in self._hooks):
                    self._hooks[nrw] = func

        return func

    def invoke(self, hook_name, *args, **kwargs):
        if hook_name in self._hooks:
            hnd = self._hooks[hook_name]

            return hnd(*args, **kwargs)
        else:
            raise HookNotFound(self, hook_name, args, kwargs)

class WatchDog(Primitive):
    def report_data(self, aspect, **payload):
        return self._mgr.report_data(self.narrow, aspect, **payload)

    
class Observer(Primitive):
    pass

class Sparkle(object):
    def __init__(self):
        self._observe = {}
        self._canins  = {}

    def register_observer(self, *args, **kwargs):
        def do_apply(handler, name, title=None):
            if name not in self._observe:
                self._observe[name] = dict(
                    title   = title,
                    handler = handler,
                )

            return handler

        return lambda handler: do_apply(handler, *args, **kwargs)

    def resolve_observer(self, narrow):
        if narrow in self._observe:
            return self._observe[narrow]
        else:
            return None

    def register_dog(self, *args, **kwargs):
        def do_apply(handler, name, title=None):
            if name not in self._canins:
                self._canins[name] = dict(
                    title   = title,
                    handler = handler,
                )

            return handler

        return lambda handler: do_apply(handler, *args, **kwargs)

    def resolve_dog(self, narrow):
        if narrow in self._canins:
            return self._canins[narrow]
        else:
            print self._canins.keys()

            return None

Sparkle = Sparkle()

class Manager(object):
    def __init__(self, narrow):
        self._nrw     = narrow
        self._observe = {}
        self._canin   = {}

    observers = property(lambda self: self._observe.values())
    canins    = property(lambda self: self._canin.values())

    def feed(self, nrw, alias, *args, **kwargs):
        if alias not in self._observe:
            meta = Sparkle.resolve_observer(nrw)

            if meta:
                self._observe[alias] = meta['handler'](self, alias, *args, **kwargs)
            else:
                raise Exception("No observer with name : %s\n\n%s" % (nrw,meta))

        return self._observe.get(alias, None)

    def watch(self, nrw, alias, *args, **kwargs):
        if alias not in self._canin:
            meta = Sparkle.resolve_dog(nrw)

            if meta:
                self._canin[alias] = meta['handler'](self, alias, *args, **kwargs)
            else:
                raise Exception("No canin race with name : %s\n\n%s" % (nrw,meta))

        return self._canin.get(alias, None)

    def report_data(self, narrow, aspect, **payload):
        for observer in self.observers:
            if hasattr(observer, 'report'):
                observer.report(narrow, aspect, **payload)

    def asyncloop(self):
        for observer in self.observers:
            if hasattr(observer, 'prepare'):
                observer.prepare()

        if len(self.canins)!=0:
            while True:
                for dog in self.canins:
                    dog.watch()
        else:
            print "No canins found. Get some. They are lovely and handy !"
