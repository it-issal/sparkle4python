#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

import requests

###############################################################################

@Sparkle.register_observer('http-post', "HTTP-POST sender")
class HttpPostSender(Observer):
    def initialize(self, verb, link, query=""):
        self._verb  = verb
        self._link  = link
        self._query = query

    verb  = property(lambda self: self._verb)
    link  = property(lambda self: self._link)
    query = property(lambda self: self._query)

    def connect(self):
        pass

    def escape(self, value, **context):
        for key in context:
            flag = "${%s}" % key

            while flag in value:
                vaue = value.replace(flag, context[key])

        return value

    def report(self, narrow, aspect, **payload):
        cnx   = getattr(requests, self.verb.lower(), None)

        if cnx:
            cnt = dict(
                narrow=narrow,
                aspect=aspect,
            )

            try:
                target = self.link % narrow
            except:
                target = self.link

            qs = dict([
                (k,self.escape(v, **cnt))
                for k,v in self.query.iteritems()
            ])

            cnx(target, params=qs, data=payload)
        else:
            print "Unsupported HTTP verb : %s" % self.verb
