#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

import collectd

###############################################################################

@Sparkle.register_observer('collectd', "Collectd sender")
class CollectedSender(Observer):
    def initialize(self, host='localhost', port=25826):
        self._host = host
        self._port = port

    def connect(self):
        self._cnx   = collectd.Connection(self.manager.alias, self._host, self._port)

        return self._cnx

    def prepare(self):
        collectd.start_threads()

    def send(self, narrow, aspect, **payload):
        instance = getattr(self._cnx, narrow, None)

        if instance:
            instance.record(aspect, **payload)
