#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

import serial

###############################################################################

@Sparkle.register_dog('jarvis-tty', "Jarvis TTy")
class JarvisTTy(WatchDog):
    def initialize(self, device, baud=9600, refresh=2.0):
        self._dev   = device
        self._baud  = baud
        self._cycle = refresh

        self._hooks = {}

        self.reset()

    def reset(self):
        self._tty   = serial.Serial(self._dev, baudrate=self._baud, timeout=self._cycle)
        self._buff  = ""

    def watch(self):
        ch = port.read()

        if ch in ('\r','\n'):
            if len(raw):
                if "#" in raw:
                    cmd,params = raw.split('#', 1)

                    params = dict([
                        x.split(':',1)
                        for x in params.split('|')
                    ])

                    try:
                        self.invoke(cmd, **params)
                    except HookNotFound,ex:
                        print "<%s>\t%s" % (ex.hook_name, repr(ex.kwargs))
                else:
                    print "error> Unsupported format : '%s'" % raw

            raw = ""
        else:
            raw += ch
