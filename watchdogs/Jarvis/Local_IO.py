#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

###############################################################################

@Sparkle.register_dog('rpi-weather', "Raspberry Pi :: Weather")
class RPi_Weather(WatchDog):
    DHT_mapping = {
        11:   Adafruit_DHT.DHT11,
	22:   Adafruit_DHT.DHT22,
	2302: Adafruit_DHT.AM2302,
    }

    def initialize(self, dht=None, survey_pins=None, rfid_model=None, lcd_addr=None):
        self._survey = dict([
            (pin, dict(heap=None))
            for pin,key in (survey_pins or [])
            if pin in (4, 17, 18, 23, 24)
        ])

        for pin in self._survey:
            GPIO.setup(pin, GPIO.IN)
        
        if dht is None:
            self._dht = None
        else:
            self._dht = self.DHT_mapping[dht['model']], dht['pin']
        
        self._rfid = None

        if rfid_model=='mfrc522':
            hnd = getattr(sensors.RFID, rfid_model.upper(), None)
            
            if callable(hnd):
                self._rfid = hnd()
        
        if lcd_addr is None:
            self._lcd = None
        else:
            self._lcd = lcd_addr

    def watch(self):
        self.read_weather()

        self.read_rfid()
        
        for pin,cfg in self._survey.iteritems():
            nv = False # GPIO.input(pin)
            
            if (cfg['heap'] is None) and nv==True:
                cfg['heap'] = datetime.now()
            elif (cfg['heap'] is not None) and nv==False:
                self.report_data('movement', start=cfg['heap'], end=datetime.now())
                
                cfg['heap'] = None
            
            self._survey[pin] = cfg

    #**************************************************************************

    DHT_type = property(lambda self: self._dht[0])
    DHT_pin  = property(lambda self: self._dht[1])

    def read_weather(self):
        if self._dht is not None:
            h, t = Adafruit_DHT.read_retry(self.DHT_type, self.DHT_pin)

            if h is not None and t is not None:
                self.report_data('weather', humid=h, temp=t)

                print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(t, h)
            else:
                print 'Failed to get reading. Try again!'

    #**************************************************************************

    RFID     = property(lambda self: self._rfid)

    def read_rfid(self):
        if self.RFID:
            # Scan for cards    
            (status,TagType) = self._rfid.MFRC522_Request(self._rfid.PICC_REQIDL)

            # If a card is found
            if status == self._rfid.MI_OK:
                print "Card detected"
    
            # Get the UID of the card
            (status,uid) = self._rfid.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == self._rfid.MI_OK:
                # Print UID
                print "Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
    
                # This is the default key for authentication
                key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
        
                # Select the scanned tag
                self._rfid.MFRC522_SelectTag(uid)

                # Authenticate
                status = self._rfid.MFRC522_Auth(self._rfid.PICC_AUTHENT1A, 8, key, uid)

                # Check if authenticated
                if status == self._rfid.MI_OK:
                    self._rfid.MFRC522_Read(8)
                    self._rfid.MFRC522_StopCrypto1()
                else:
                    print "Authentication error"
