#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

import Adafruit_DHT

import RPi.GPIO

###############################################################################

@Sparkle.register_dog('rpi-weather', "Raspberry Pi :: Weather")
class RPi_Weather(WatchDog):
    DHT_mapping = {
        '11':   Adafruit_DHT.DHT11,
		'22':   Adafruit_DHT.DHT22,
		'2302': Adafruit_DHT.AM2302,
    }

    def initialize(self, dht=None, survey_pins=[], lcd_addr=None, rfid_model=None):
        if dht is None:
            self._dht = None
        else:
            self._dht = self.DHT_mapping[dht['model']], int(dht['pin'])

        self._survey = [
            dict(pin=x, since=None)
            for x in survey_pins
            if x in range(0, 28)
        ]

        self._lcd  = None
        self._rfid = None

    def watch(self):
        if self._dht is not None:
            h, t = Adafruit_DHT.read_retry(self._dht[0], self.dht[1])

            if h is not None and t is not None:
                self.invoke('weather', humid=h, temp=t)

            	print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(t, h)
            else:
            	print 'Failed to get reading. Try again!'

        for detect in self._survey:
            state = False

            if (state is True) and (detect['since'] is None):
                detect['since'] = datetime.now()
            elif (state is False) and (detect['since'] is not None):
                self.invoke('weather', pin=detect[pin], since=detect['since'], until=datetime.now())

                detect['since'] = None

        if self._lcd is not None:
            pass

        if self._rfid is not None:
            pass
